"""
app.py:
    initializes and registers app components, 
    loads configuration, 
    and sets up migration util
"""


from flask import Flask
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager

from orderApi.api import api
from orderApi.Shared import db

# initialize flask app
app = Flask("OrderApi")

# load config from config file
app.config.from_object('config.Config')

# register api blueprint
app.register_blueprint(api, url_prefix="/api")

# initialize database
db.init_app(app)

# set up migrations
migrate = Migrate(app, db)

# initalize JWT util
jwt = JWTManager(app)
