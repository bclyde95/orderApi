"""
order.py:
    data classes and orm binding for Order functions
"""

from datetime import datetime

# Import db
from .Shared import db

class Order(db.Model):
    """ Defines Order functionality, links to customers, restaurants, and items """
    __tablename__ = 'orders'

    id = db.Column(db.Integer, primary_key=True)
    customerId = db.Column(db.Integer, db.ForeignKey('customers.id'))
    restaurantId = db.Column(db.Integer, db.ForeignKey('restaurants.id'))
    customer = db.relationship('Customer', backref='order', lazy=False)
    restaurant = db.relationship('Restaurant', backref='order', lazy=False)
    date = db.Column(db.DateTime, default=datetime.now)
    paid = db.Column(db.Boolean)
    notes = db.Column(db.Text)
    items = db.relationship('OrderItem', backref="order", lazy=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict(orderId=self.id,
        restaurantId=self.restaurant.restaurantId,
        restaurantName=self.restaurant.name,
        customerName=self.customer.contact.firstName,
        customerPhones=[phone.getValue() for phone in self.customer.contact.phones],
        datetime=self.date(),
        paid=self.paid,
        notes=self.notes,
        items=[item.toDict() for item in self.items]
        )
    

class OrderItem(db.Model):
    """ Associates a MenuItem with a specific order in a certain quantity """
    __tablename__ = 'orderitems'

    id = db.Column(db.Integer, primary_key=True)
    orderId = db.Column(db.Integer, db.ForeignKey('orders.id'))
    itemId  = db.Column(db.Integer, db.ForeignKey('menuitems.id'))
    quantity = db.Column(db.Integer, nullable=False)
    item = db.relationship('MenuItem', backref='orderitem', lazy=False)
    
    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict(self.item.toDict(),
        quantity=self.quantity
        )
