"""
Restaurant.py:
    Data classes and orm binding for Restaurant functions
"""

from datetime import datetime

# Import db and Order model
from .Shared import db
from .Order import Order


class Restaurant(db.Model):
    """ Master class for restaurant functionality, links all subclasses """
    __tablename__ = 'restaurants'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.VARCHAR(50), nullable=False)
    restaurantId = db.Column(db.VARCHAR(50), nullable=False)
    password = db.Column(db.VARCHAR(50), nullable=False)
    joined = db.Column(db.DateTime, default=datetime.utcnow)
    description = db.Column(db.Text)
    categories = db.relationship('Category', backref='restaurant', lazy=False)
    contact = db.relationship('RestaurantContact', backref='restaurant', lazy=False)
    menu = db.relationship('Menu', backref='restaurant', lazy=False)
    orderList = db.relationship('Order', backref='_restaurant', lazy=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict(name=self.name,
        restaurantId=self.restaurantId,
        joined=self.joined,
        description=self.description,
        categories=[category.toDict() for category in self.categories],
        contact=self.contact.toDict(),
        menu=self.menu.getMenu()
        )

    @classmethod
    def getByRestaurantId(cls, restaurantId):
        """ returns Restaurant if the restaurantId in the class is in the database. NULL if not """
        return cls.query.filter(cls.restaurantId == restaurantId).first()

    def commitToDb(self):
        db.session.add(self)
        db.session.commit()

class Category(db.Model):
    """ Defines categories that restaurants can attribute """
    __tablename__ = 'categories'

    id = db.Column(db.Integer, primary_key=True)
    restaurantId = db.Column(db.Integer, db.ForeignKey('restaurants.id'))
    name = db.Column(db.VARCHAR(50), nullable=False)

    def getValue(self):
        return self.name


###     Contact Classes     ###

class RestaurantContact(db.Model):
    """ Builds out contact functionality for restaurants, using shared address and phone number classes """
    __tablename__ = 'restaurantcontacts'

    id = db.Column(db.Integer, primary_key=True)
    restaurantId = db.Column(db.Integer, db.ForeignKey('restaurants.id'))
    modified = db.Column(db.DateTime)
    email = db.Column(db.VARCHAR(50), nullable=False)
    addresses = db.relationship('Address', backref='restaurantContact', lazy=False)
    phones = db.relationship('Phone', backref='restaurantContact', lazy=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict(email=self.email,
        addresses=[address.toDict() for address in self.addresses],
        phones=[phone.toDict() for phone in self.phones],
        modified=self.modified
        )



###     Menu Classes    ###

class Menu(db.Model):
    """ Combines MenuItems into a single Menu """
    __tablename__ = 'menus'

    id = db.Column(db.Integer, primary_key=True)
    restaurantId = db.Column(db.Integer, db.ForeignKey('restaurants.id'))
    modified = db.Column(db.DateTime)
    menuItems = db.relationship('MenuItem', backref='menu', lazy=False)

    def getMenu(self):
        """ Constructs an array of item dicts """
        return [item.toDict() for item in self.menuItems]


class MenuItem(db.Model):
    """ Defines individual items of a menu, relating to ingredient and option tables """
    __tablename__ = 'menuitems'

    id = db.Column(db.Integer, primary_key=True)
    menuId = db.Column(db.Integer, db.ForeignKey('menus.id'))
    modified = db.Column(db.DateTime)
    name = db.Column(db.Text, nullable=False)
    price = db.Column(db.Float, nullable=False)
    ingredients = db.relationship('Ingredient', backref='menuitem', lazy=False)
    options = db.relationship('Option', backref='menuitem', lazy=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict({"name" : self.name},
        price=self.price,
        ingredients=[ingredient.toDict() for ingredient in self.ingredients],
        options=[option.toDict() for option in self.options],
        modified=self.modified
        )


class Ingredient(db.Model):
    """ Defines an ingredient for use in MenuItem """
    __tablename__ = 'ingredients'

    id = db.Column(db.Integer, primary_key=True)
    itemId = db.Column(db.Integer, db.ForeignKey('menuitems.id'))
    modified = db.Column(db.DateTime)
    name = db.Column(db.Text, nullable=False)
    extraPrice = db.Column(db.Float, default='0.50')
    choices = db.relationship('Choice', backref='ingredient', lazy=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict(name=self.name,
        choices=[choice.toDict() for choice in self.choices],
        modified=self.modified
        )


class Option(db.Model):
    """ Defines functionality to add optional ingredients to MenuItems """
    __tablename__ = 'options'

    id = db.Column(db.Integer, primary_key=True)
    itemId = db.Column(db.Integer, db.ForeignKey('menuitems.id'))
    modified = db.Column(db.DateTime)
    name = db.Column(db.Text, nullable=False)
    price = db.Column(db.Float, default='0.00')
    extraPrice = db.Column(db.Float, default='0.50')
    choices = db.relationship('Choice', backref='option', lazy=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict(name=self.name,
        price=self.price,
        choices=[choice.getValue() for choice in self.choices],
        modified=self.modified
        )


class Choice(db.Model):
    """ Defines the choices available for ingredients and options """
    __tablename__ = 'choices'

    id = db.Column(db.Integer, primary_key=True)
    ingredientId = db.Column(db.Integer, db.ForeignKey('ingredients.id'))
    optionId = db.Column(db.Integer, db.ForeignKey('options.id'))
    name = db.Column(db.Text, nullable=False)

    def getValue(self):
        """ Returns the choice name """
        return self.name
