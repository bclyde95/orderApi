"""
api.py:
    routing and api CRUD functions
"""

# library imports
from flask import Blueprint, jsonify, request
from flask_jwt_extended import create_access_token, create_refresh_token 
from flask_jwt_extended import jwt_required 
from flask_jwt_extended import get_jwt_identity 

# file imports
from .UtilFunctions import hashPassword, checkPassword
from .Shared import db, Phone, Address
from .Restaurant import Restaurant, Category, RestaurantContact
from .Restaurant import MenuItem, Ingredient, Option, Choice
from .Customer import Customer, CustomerContact
from .Order import Order, OrderItem

api = Blueprint('api', __name__)


""" Endpoint for restaurants to login and recieve an API token """
@api.route('/auth/restaurantlogin', methods=['POST'])
def restaurantlogin():
    """ Takes in a POST request with login info, verifies the login, and returns an API access token """
    data = request.get_json()
    # Tries to find Restaurant in database, returns User does not exist error if not.
    restaurant = Restaurant.getByRestaurantId(data["restaurantId"])
    if restaurant:
        ## Checks password with password in db, returns access and refresh tokens if successful. Error if not.
        if checkPassword(restaurant.password, data["password"]):
            access_token = create_access_token(identity = restaurant.id)
            refresh_token = create_refresh_token(identity = restaurant.id)
            return jsonify({
                'status' : 'success',
                'accessToken' : access_token,
                'refreshToken' : refresh_token
            })
        else:
            return jsonify({
                'status' : 'error', 
                'message' : 'Incorrect password'
            })
    if not restaurant:
        return jsonify({
            'status' : 'error', 
            'message' : 'User does not exist'
        }), 404
    else:
        return jsonify({
            'status' : 'error', 
            'message' : 'Something went wrong'
        }), 500


""" Endpoint for customers to login and recieve an API token """
@api.route('/auth/customerlogin', methods=['POST'])
def customerlogin():
    """ Takes in a POST request with login info, verifies the login, and returns an API access token """
    data = request.get_json()
    # Tries to find Customer in database, returns User does not exist error if not.
    customer = Customer.getByUsername(data["username"])
    if customer:
        ## Checks password with password in db, returns access and refresh tokens if successful. Error if not.
        if checkPassword(customer.password, data["password"]):
            access_token = create_access_token(identity = data["username"])
            refresh_token = create_refresh_token(identity = data["username"])
            return jsonify({
                'status' : 'success',
                'accessToken' : access_token,
                'refreshToken' : refresh_token
            })
        else:
            return jsonify({
                'status' : 'error', 
                'message' : 'Incorrect password'
            })
    if not customer:
        return jsonify({
            'status' : 'error', 
            'message' : 'User does not exist'
        }), 404
    else:
        return jsonify({
            'status' : 'error', 
            'message' : 'Something went wrong'
        }), 500


""" Enpoint for customers to register and recieve inital API token """
@api.route('/auth/customerregister', methods=['POST'])
def register():
    """ Takes in a POST request with registration info, verifies that registration is valid, creates a db entry, and returns an API access token """
    data = request.get_json()

    # Checks if username exist and return error if so
    if Customer.getByUsername(data["username"]):
        return jsonify({
            'status' : 'error',
            'message' : 'Username already in use'
        })

    # initialize new Customer
    newCustomer = Customer(
        userName=data["username"],
        password=hashPassword(data["password"]),
    )

    #initialize contact info
    contact = CustomerContact(
        firstName=data["firstName"],
        lastName=data["lastName"],
        email=data["email"]
    )

    # Add addresses to contact info
    addresses = []
    for a in data["addresses"]:
        address = Address(
           address=a["address"],
           city=a["city"],
           state=a["state"],
           zipcode=a["zipcode"]
        )
        addresses.append(address)
    contact.addresses = addresses

    # Add phones to contact info
    contact.phones = [Phone(phoneNumber=phone) for phone in data["phones"]]

    # add contact info to customer
    newCustomer.contact = contact

    # Commit data to db. If successful, return access and refresh tokens. If not, return error
    try:
        newCustomer.commitToDb()
        access_token = create_access_token(identity = data["username"])
        refresh_token = create_refresh_token(identity = data["username"])
        return jsonify({
            'status' : 'success',
            'accessToken' : access_token,
            'refreshToken' : refresh_token
        })
    except ValueError:
        return jsonify({
            'status' : 'error',
            'message' : 'Something went wrong'
        }), 500
    
    
   

""" Enpoint to refresh API tokens """
@api.route('/auth/tokenrefresh', methods=['POST'])
@jwt_required(refresh=True)
def tokenRefresh(self):
    """ Takes in refresh token and returns new access token"""
    user = get_jwt_identity()
    access_token = create_access_token(identity = current_user)
    try:
        return jsonify({
            'status' : 'success', 
            'accessToken': access_token
        })
    except Exception:
        return jsonify({
            'status' : 'error',
            'message' : 'Something went wrong'
        }), 500


""" Enpoint to get all restaurants or individual restaurants """
@api.route('/restaurants/', methods=['GET'])
@api.route('/restaurants/<string:restaurantId>')
def restaurant(restaurantId=None):
    """ Returns all restaurants if no Id supplied, and individual restaurants if restaurantId != None """
    if restaurantId:
        restaurant = Restaurant.query.filter(Restaurant.restaurantId == restaurantId).first()
        if restaurant:
            return jsonify({
                'status' : 'success',
                'restaurant' : restaurant.toDict()
            })
    restaurants = Restaurant.query.all()
    return jsonify({
        'status' : 'success',
        'restaurants' : [restaurant.toDict() for restaurant in restaurants]
    })


""" Enpoint to get all orders or orders per restaurant ***Must have access token*** """
@api.route('/orders/')
@api.route('orders/<int:idIn>')
@jwt_required(refresh=False)
def order(idIn=None):
    if idIn:
        orders = Order.query.filter(Order.restaurantId == idIn).all()
    orders = Order.query.all()
    return jsonify({'orders' : [order.toDict() for order in orders]})


""" Enpoint to get all customers or individual customers """
@api.route('/customers/', methods=['GET'])
@api.route('/customers/<string:user>', methods=['GET'])
def customer(user=None):
    """ Returns all customers if no user supplied, and individual restaurants if user != None """
    if user:
        customer = Customer.query.filter(Customer.userName == user).first()
        if customer:
            return jsonify({
                'status' : 'success',
                'customer' : customer.toDict()
            })
        else:
            return jsonify({
                'status' : 'error',
                'message' : 'User does not exist'
            })
    customers = Customer.query.all()
    return jsonify({
        'status' : 'success',
        'customers' : [customer.toDict() for customer in customers]
    })
