"""
Customer.py:
     Data classes and ORM binding for Customer functions
"""

from werkzeug.security import check_password_hash
from datetime import datetime

#import db
from .Shared import db


class Customer(db.Model):
    """ Defines customer functionality, links contact model """
    __tablename__ = 'customers'

    id = db.Column(db.Integer, primary_key=True)
    userName = db.Column(db.VARCHAR(25), nullable=False)
    password = db.Column(db.String(250), nullable=False)
    joined = db.Column(db.DateTime, default=datetime.now)
    contactId = db.Column(db.Integer, db.ForeignKey('customercontacts.id'))
    contact = db.relationship('CustomerContact', backref=db.backref('customer', uselist=False))
    orders = db.relationship('Order', backref='_customer', lazy=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict(userName=self.userName,
        password=self.password,
        joined=self.joined,
        contact=self.contact.toDict(),
        orders=[order.toDict() for order in self.orders]
        )

    @classmethod
    def getByUsername(cls, username):
        """ returns Customer if the userName in the class is in the database. NULL if not """
        return cls.query.filter(cls.userName == username).first()

    def commitToDb(self):
        db.session.add(self)
        db.session.commit()

class CustomerContact(db.Model):
    """ Builds out contact functionality for customers, using shared address and phone number classes """
    __tablename__ = 'customercontacts'

    id = db.Column(db.Integer, primary_key=True)
    firstName = db.Column(db.VARCHAR(25), nullable=False)
    lastName = db.Column(db.VARCHAR(25), nullable=False)
    email = db.Column(db.VARCHAR(50), nullable=False)
    addresses = db.relationship('Address', backref='customercontact', lazy=False)
    phones = db.relationship('Phone', backref='customercontact', lazy=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict(firstname=self.firstName,
        lastName=self.lastName,
        email=self.email,
        addresses=[address.toDict() for address in self.addresses],
        phones=[phone.getValue() for phone in self.phones]
        )
