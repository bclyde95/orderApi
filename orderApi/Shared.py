"""
Shared.py:
    Database init and shared data classes
"""

from flask_sqlalchemy import SQLAlchemy

# init db
db = SQLAlchemy()

""" Maps Address functionality as a class and a table in db """
class Address(db.Model):
    """ Address model with distinct links to restaurant and customer contact tables """
    __tablename__ = 'addresses'

    id = db.Column(db.Integer, primary_key=True)
    rContactId = db.Column(db.Integer, db.ForeignKey('restaurantcontacts.id'))
    cContactId= db.Column(db.Integer, db.ForeignKey('customercontacts.id'))
    modified = db.Column(db.DateTime)
    address = db.Column(db.Text, nullable=False)
    city = db.Column(db.Text, nullable=False)
    state = db.Column(db.VARCHAR(2), nullable=False)
    zipcode = db.Column(db.VARCHAR(16), nullable=False)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict(
        address=self.address,
        city=self.city,
        state=self.state,
        zipcode=self.zipcode
        )


""" Maps Address functionality as a class and a table in db """
class Phone(db.Model):
    """ Phone model with distinct links to restaurant and customer contact tables """
    __tablename__ = 'phones'
    
    id = db.Column(db.Integer, primary_key=True)
    rContactId = db.Column(db.Integer, db.ForeignKey('restaurantcontacts.id'))
    cContactId = db.Column(db.Integer, db.ForeignKey('customercontacts.id'))
    phoneNumber = db.Column(db.VARCHAR(16), nullable=False)

    def getValue(self):
        return self.phoneNumber
