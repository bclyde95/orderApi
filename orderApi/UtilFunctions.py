"""
UtilFunctions.py:
    houses function declarations that are used in other files
"""

from werkzeug.security import generate_password_hash, check_password_hash


def hashPassword(password):
    return generate_password_hash(password)

def checkPassword(hashed, password):
    return check_password_hash(hashed, password) 
